<!-- jQuery -->
<script src="{!! url('bower_components/jquery/dist/jquery.min.js') !!}"></script>
<!-- Knockout JS -->
<script src="{!! url('bower_components/knockout/dist/knockout.js') !!}"></script>
<!-- Bootstrap -->
<script src="{!! url('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! url('bower_components/AdminLTE/dist/js/app.min.js') !!}"></script>
<!-- DataTables -->
<script src="{!! url('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! url('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') !!}"></script>
<!-- FastClick -->
<script src="{!! url('bower_components/AdminLTE/plugins/fastclick/fastclick.min.js') !!}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{!! url('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script src="{!! url('bower_components/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! url('bower_components/bootstrap-sweetalert/dist/sweetalert.min.js') !!}"></script>
<script src="{!! url('js/admin/custom-config.js') !!}"></script>
